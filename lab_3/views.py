from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendFrom
from django.http import HttpResponseRedirect
# Create your views here.
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def addFriend(request):
    if request.method == "POST" :
        forms = FriendFrom(request.POST or None)
        if forms.is_valid():
            forms.save()
            return HttpResponseRedirect("/lab-3")
    # forms = FriendFrom()
    # response = {'form' : forms}
    return render(request, 'lab3_forms.html')
    
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

def index(request):
    note = Note.objects.all()  
    response = {'Note': note}
    return render(request, 'lab4_index.html', response)

def addNote(request):
    if request.method == "POST" :
        forms = NoteForm(request.POST or None)
        if forms.is_valid():
            forms.save()
            return HttpResponseRedirect("/lab-4")
    return render(request, 'lab4_forms.html')
    
def note_list(request):
    note = Note.objects.all()  
    response = {'Note': note}
    return render(request, 'lab4_note_list.html', response)
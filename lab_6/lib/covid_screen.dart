import 'package:flutter/material.dart';

class Covid19 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
            children: List.generate(37, (index) {
          if (index == 0) {
            return Container(
              height: 100,
              width: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Covid_update.png'),
                ),
              ),
            );
          } 
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    padding: EdgeInsets.all(10.0),
                    child: Placeholder(
                      fallbackHeight: 100.0,
                    ),
                  ),
                  Text('Card ke-$index'),
                ],
              ),
            ),
          );
        })),
      ),
    );
  }
}

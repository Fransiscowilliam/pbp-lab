1. Apakah perbedaan antara JSON dan XML?
XML merupakan markup language yang menambahkan informasi tambahan ke teks biasa, sedangkan JSON dapat mewakili objek data dan lebih efisien dalam merepresentasikan data. Dari sisi sintaks, JSON lebih mudah dibaca dan dimengerti dibandingkan XML. JSON lebih berorientasi ke data, sedangkan XML lebih berorientasi ke dokumen.

Berikut contoh kode XML
<django-objects version="1.0">
    <object model="lab_2.note" pk="1">
        <field name="to" type="CharField">Frans</field>
        <field name="froms" type="CharField">William</field>
        <field name="title" type="CharField">Test</field>
        <field name="message" type="CharField">ini cuma ngetest</field>
    </object>
</django-objects>

Berikut contoh kode JSON
[
    {
    "model": "lab_2.note", 
    "pk": 1, 
    "fields": {"to": "Frans", "froms": "William", "title": "Test", "message": "ini cuma ngetest"}}
]

2. Apakah perbedaan antara HTML dan XML?
XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data. Dari sisi sintaks, HTML lebih mudah dimengerti dan lebih sederhana dibandingkan XML. Selain itu, tag pada HTML terbatas sedangkan tag pada XML dapat dikembangkan.

Berikut contoh kode HTML
<!DOCTYPE html>
<html>
    <body>
        <h1>My First Heading</h1>
        <p>My first paragraph.</p>
    </body>
</html>

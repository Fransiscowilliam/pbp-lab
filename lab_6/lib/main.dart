// @dart=2.9

import 'package:flutter/material.dart';
import './covid_screen.dart';
void main() => runApp(MyApp());

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'D06',
      theme: ThemeData(
        primarySwatch: createMaterialColor(Color(0xFF881D1D)),
        canvasColor: Color.fromRGBO(237, 224, 208, 1),
        fontFamily: 'Raleway',
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Home> {
  int _currentIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    Text('Home'),
    Text('Workout'),
    Text('Recipe'),
    Covid19(),
    Text('Forum'),
    Text('Login'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("D06"),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xFFB29576),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_rounded),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            // backgroundColor: _currentIndex == 1 ? Color(0xFF881D1D) : Color(0xFFB29576),
            icon: Icon(Icons.directions_run_sharp),
            title: Text('Workout'),
          ),
          BottomNavigationBarItem(
            // backgroundColor: _currentIndex == 2 ? Color(0xFF881D1D) : Color(0xFFB29576),
            icon: Icon(Icons.receipt_long_rounded),
            title: Text('Recipe'),
          ),
          BottomNavigationBarItem(
            // backgroundColor: _currentIndex == 3 ? Color(0xFF881D1D) : Color(0xFFB29576),
            icon: Icon(Icons.list_alt_rounded),
            title: Text('Covid-19'),
          ),
          BottomNavigationBarItem(
            // backgroundColor: _currentIndex == 4 ? Color(0xFF881D1D) : Color(0xFFB29576),
            icon: Icon(Icons.people_alt_rounded),
            title: Text('Forum'),
          ),
          BottomNavigationBarItem(
            // backgroundColor: _currentIndex == 5 ? Color(0xFF881D1D) : Color(0xFFB29576),
            icon: Icon(Icons.person_pin_circle),
            title: Text('Login'),
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}

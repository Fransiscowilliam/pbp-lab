import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  LoginPage createState() => LoginPage();
}

class LoginPage extends State<Home> {
  final formKey = GlobalKey<FormState>(); //key for form
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    var confirmPass;
    String fullName = "";
    String nickName = "";
    String password = "";

    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        backgroundColor: Color(0xFFffffff),
        body: Container(
          padding: const EdgeInsets.only(left: 40, right: 40),
          child: Form(
            key: formKey, //key for form

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Welcome !",
                  style: TextStyle(fontSize: 30, color: Color(0xFF363f93)),
                ),
                SizedBox(height: height * 0.04),
                TextFormField(
                  onSaved: (value) {
                    fullName = value!;
                  },
                  decoration: InputDecoration(
                    labelText: "Enter your full name",
                    hintText: 'Example : Fransisco William',
                  ),
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value!)) {
                      return "Enter correct full name";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                TextFormField(
                  onSaved: (value) {
                    nickName = value!;
                  },
                  decoration: InputDecoration(
                    labelText: "Enter your nick name",
                    hintText: 'Example : Frans',
                  ),
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value!)) {
                      return "Enter correct nick name";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                TextFormField(
                  onSaved: (value) {
                    password = value!;
                  },
                  decoration: InputDecoration(
                    labelText: "Enter your password",
                    hintText: 'Example : Exampl3@example',
                  ),
                  obscureText: true,
                  validator: (value) {
                    confirmPass = value;
                    if (value!.isEmpty) {
                      return "Enter correct password";
                    } else {
                      if (!RegExp(
                              r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                          .hasMatch(value!)) {
                        return "Please insert atleast 1 Upper case, 1 Lower case, \n1 Numeric number, 1 Special Character, and 8 Characters \nin length";
                      } else {
                        return null;
                      }
                    }
                  },
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Confirm your password",
                    hintText: 'Insert same password',
                  ),
                  obscureText: true,
                  // controller: _confirmPass,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter correct password";
                    } else {
                      if (value != confirmPass) {
                        return "Password not match";
                      } else {
                        return null;
                      }
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        formKey.currentState!.save();
                        print(fullName);
                        print(nickName);
                        print(password);
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Processing Data...')),
                        );
                      }
                    },
                    child: const Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

from django.urls import path
from .views import index, addNote, note_list
from .models import Note

urlpatterns = [
    path('', index, name='index'),
    path('add-note', addNote, name="add-note"),
    path('note-list', note_list, name="note-list"),
]
